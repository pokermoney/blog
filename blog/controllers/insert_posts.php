<?php
	$mysqli = new mysqli("localhost","root","","blog");
	$mysqli->query('SET NAMES UTF8');

	$target_dir = "../img/uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            $qry = "INSERT INTO posts (username,titles,details,pictures,dates,catogory,time) VALUES ('".addslashes($_POST['username'])."' , '".addslashes($_POST['title'])."','".addslashes($_POST['details'])."','".basename($_FILES["fileToUpload"]["name"])."','".addslashes($_POST['date'])."','".addslashes($_POST['catogory'])."','".addslashes($_POST['times'])."')";
            echo $qry;
            $mysqli->query($qry);
            header('Location: ../index.php');
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
	header('Location: ../index.php');
?>