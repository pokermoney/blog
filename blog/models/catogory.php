<?php
  class Catogory{
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $username;
    public $titles;
    public $pic;
    public $timestamp;
    public $catogory;

    public function __construct($id, $username, $titles,$pictures,$times,$cat) {
      $this->id      = $id;
      $this->username  = $username;
      $this->titles = $titles;
      $this->pic = $pictures;
      $this->timestamp = $times;  
      $this->catogory = $cat;
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM posts');
      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $cat) {
        $timestamp = $cat['dates'].' '.$cat['time'];
        $list[] = new Catogory($cat['id'], $cat['username'], $cat['titles'],$cat['pictures'],$timestamp,$cat['catogory']);  
      }
      return $list;
    }
  }
?>