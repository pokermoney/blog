<?php
  class Comment {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $author;
    public $content;
    public $postid;
    public $timestamp;

    public function __construct($id, $author, $content,$postid,$times) {
      $this->id      = $id;
      $this->author  = $author;
      $this->content = $content;
      $this->postid = $postid;
      $this->timestamp = $times;  
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM posts');
      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $comment) {
        $timestamp = $comment['date'].' '.$comment['times'];
        $list[] = new Comment($comment['id'], $comment['author'], $comment['content'],$comment['postid'],$timestamp);
      }
      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();
      // we make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM comment WHERE id_posts = :id');
      // the query was prepared, now we replace :id with our actual $id value
      $req->execute(array('id' => $id));
      $list = [];
    	foreach($req->fetchAll() as $comment) {
        $timestamp = $comment['date'].' '.$comment['times'];
        $list[] = new Comment($comment['id'], $comment['author'], $comment['comments'],$comment['id_posts'],$timestamp);
    	}
     // print_r($list);
        return $list;
  }
}
?>