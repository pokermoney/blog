<?php
  class Post {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $username;
    public $titles;
    public $pic;
    public $timestamp;

    public function __construct($id, $username, $titles,$pictures,$times) {
      $this->id      = $id;
      $this->username  = $username;
      $this->titles = $titles;
      $this->pic = $pictures;
      $this->timestamp = $times;  
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM posts');
      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $timestamp = $post['dates'].' '.$post['time'];
        $list[] = new Post($post['id'], $post['username'], $post['titles'],$post['pictures'],$timestamp);
      }
      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();
      // we make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM posts WHERE id = :id');
      // the query was prepared, now we replace :id with our actual $id value
      $req->execute(array('id' => $id));
      $post = $req->fetch();
      $timestamp = $post['dates'].' '.$post['time'];

      return new Post($post['id'], $post['username'], $post['titles'], $post['pictures'],$timestamp);
    }
  }
?>