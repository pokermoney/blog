<?php
include_once '../includes/db_connect.php';
include_once '../includes/functions.php';
sec_session_start();
$mysqli = new mysqli("localhost","root","","blog");
$mysqli->query("SET NAMES 'utf8' COLLATE 'utf8_general_ci';");
$qry ="SELECT * FROM `posts` WHERE id=".$_GET['id'];
$result = $mysqli->query($qry);
$row = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html>
<head>
  <title>Blog+ | Editor</title>
  <link rel="stylesheet" type="text/css" href="../CSS/index.css">
  <link rel="stylesheet" type="text/css" href="../CSS/editor.css">
  <script src='../js/tinymce/tinymce.min.js'></script>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="CSS/index.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="../views/javascript/sidebar.js"></script>
<script>
          tinymce.init({
              selector: "#mytextarea",
              resize: false,
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste textcolor autosave save"
              ],
              toolbar: "forecolor backcolor insertfile undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
          });
  </script>

</head>
<body style="background-color: #cac8c8;">

<bar>
      <a class="NameEditor" href="/blog">Blog+</a>
      <?php
      if(login_check($mysqli) == true)
      {
        echo '<div style="float: left;margin-left: 1120px;margin-top: 0.4%;font-family: Pattaya-Regular;font-size:26px;color: white;">'.$_SESSION['username'].'</div';
      }
      ?>
      <div>
        <img class="dropdown-toggle" type="button" data-toggle="dropdown" style="float: right; width: 40px; height: 45px; margin-right: 10px" src="../img/icon/dropdown.png">
        <ul class="dropdown-menu">
          <li><a href="#">Profile</a></li>
        <?php  
        if(login_check($mysqli) == true)
        {
          echo '<li><a href="includes/logout.php">Logout</a></li>';
        }
        else
        {
          echo '<li><a href="../views/login.php">Login</a></li>';
        }
        ?>
        </ul>

      <div style="float: right; width: 3px; height: 30px; margin-top: 8px; margin-right: 10px; background-color: #ebebeb;"></div>
      </div>
    </bar>
<!--///////////////////////////////////////////////////////-->



  <form action="../controllers/update_posts.php" method="post" enctype="multipart/form-data">
    <input hidden type="text" name="username" value="<?php echo $_SESSION['username'] ?>">
    <input hidden type="text" name="id" value="<?php echo $_GET['id'] ?>">
    <div class="title" style="margin-left:1%; ">Title</div>
    
    <input class="edit_title" type="text" name="title" value="<?php echo $row['titles']?>">
    <div class="pic" style="margin-left:1%; ">Main Picture
    <input type="file" name="fileToUpload" id="fileToUpload" style="position: absolute;display: inline;"></div>
    <div style="float: left; display: inline-block; width: 10%; margin-left: 1%"><div style="font-size: 19px;">Catogory</div>
    <select name="catogory"> 
      <option value="1"
      <?php
        if($row['catogory'] == 1)
          echo ' selected';
      ?>
      >General</option>
      <option value="2" 
       <?php
        if($row['catogory'] == 2)
          echo ' selected';
      ?>
      >Travel</option>
      <option value="3"
      <?php
        if($row['catogory'] == 3)
          echo ' selected';
      ?>>Sports</option>
      <option value="4"
       <?php
        if($row['catogory'] == 4)
          echo ' selected';
      ?>
      >Food</option>
      <option value="5"
       <?php
        if($row['catogory'] == 5)
          echo ' selected';
      ?>
      >Game</option>
    </select>
    </div> 
    <div style="display: inline-block; width: 50%; margin-left: 1%; " ><textarea id="mytextarea" type="text" name="details" style="float: left; height: 300px;"><?php echo $row['titles']?></textarea></div>
    <input class="submit" type="submit" value ="Edit">
  </form>
</body>
</html>