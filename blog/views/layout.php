<?php
include_once '/includes/db_connect.php';
include_once '/includes/functions.php';
sec_session_start();
?>
<DOCTYPE html>
<html>
  <head>
    <title>Blog+ | Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="CSS/index.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="views/javascript/sidebar.js"></script>
    
  </head>
  <body>
<!--################        Tab-Bar           ###################-->
<div id="main">
    <bar>
      <div class="svg">
      <img style="position: fixed; margin:0.15%;" src="img/icon/hidden.png" onclick="toggle()">
      </div>
      <div style="position: fixed; width: 3px; height: 30px; margin-top: 8px; margin-left: 60px; background-color: #ebebeb;"></div>
      <a class="NameBlog" href='/blog'>Blog+</a><div id="balance" style="width: 160px;">
      <?php
      if(login_check($mysqli) == true)
      {
        echo '<span style="font-family: Pattaya-Regular;font-size:28px;color: white;">'.$_SESSION['username'].'</span>';
      }
      ?>
        <img class="dropdown-toggle" type="button" data-toggle="dropdown" style="float: right; width: 40px; height: 45px; margin-right: 10px" src="img/icon/dropdown.png">
        <ul class="dropdown-menu">
          <li><a href="#">Profile</a></li>
        <?php  
        if(login_check($mysqli) == true)
        {
          echo '<li><a href="includes/logout.php">Logout</a></li>';
        }
        else
        {
          echo '<li><a href="views/login.php">Login</a></li>';
        }
        ?>
        </ul>

      <div style="float: right; width: 3px; height: 30px; margin-top: 8px; margin-right: 10px; background-color: #ebebeb;"></div>
      </div>
    </bar>
<!--############################################################-->

<!--################        Side-Bar        ###################-->
    <nav>
      <div id="mySidenav" class="sidenav">
        <ul><div style="font-family: Helvetica; font-size: 30px; color: #cac8c8;">Catogory</div></br>
          <li><a href="?controller=catogory&action=index&id=1">General</a></li>
          <li><a href="?controller=catogory&action=index&id=2">Travel</a></li>
          <li><a href="?controller=catogory&action=index&id=3">Sports</a></li>
          <li><a href="?controller=catogory&action=index&id=5">Game</a></li>
          <li><a href="?controller=catogory&action=index&id=4">Food</a></li>
        </ul>
      </div>
    </nav>
<!--############################################################-->
<!--################        Post        ###################-->
    <header></header>
    <bodier>

    <div class="viewpost">
      <?php
      if(login_check($mysqli) == true)
        if($_SESSION['number_users'] == 1){
      ?>
        <div id="post">
       <a href="views/form_posts.php"><div style="float: left; font-size: 40px; margin-left: 5%; margin-top: 1%;">Create New Posts</div>
            <img style="float: left; margin-left: 8%; margin-top: 1%;" src="img/icon/writing.png"></a>
        </div>
      <?php } ?>
      <?php require_once('routes.php'); ?>
    </div>
    

    </bodier>
    </div>
    <!--<footer>
      Copyright
    </footer>-->
  <body>
<html>