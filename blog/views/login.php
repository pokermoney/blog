<?php
include_once '../includes/db_connect.php';
include_once '../includes/functions.php';
sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
    <?php 
        if(login_check($mysqli) == true)
        {
           // header("Location: php/calendar.php");
        }

    ?>
        <title>Blog+</title>
        <link rel="stylesheet" href="/blog/CSS/login.css" />
        <script type="text/JavaScript" src="../js/sha512.js"></script> 
        <script type="text/JavaScript" src="../js/forms.js"></script> 
    </head>
    
    <body>
    <!--     login  block  -->
    <login>
        <div class="login">
            <div class="NameBlog">Blog+</div>
                <div style="float: left; margin: 15%;">

                    <?php
                        if (isset($_GET['error'])) {
                        echo '<p class="error">*** Error Logging In! ***</p>';
                        }
                    ?> 
                    <form action="../includes/process_login.php" method="post" name="login_form">
                        <div class="forms"><b>Username</b><input type="text" name="username"></div>
                        <div class="forms"><b>Password</b><input type="password" name="password" id="password"></div>
                        <input class="submit click" type="button" value="Login"onclick="formhash(this.form, this.form.password);">
                    </form>
                    
                    <a href="../register.php"><button class="regis">Register</button></a>
                </div>
        </div>
        <service>
            <b><span id="welcome">Welcome To Blog</span>
            <span id="born">@ Create by apirom yaboon</span></b>
        </service>
        <a href="../index.php?controller=posts&action=index"><div class="onblog">
            <span>View Blog</span>
        </div></a>
    </login>
    </body>
</html>
